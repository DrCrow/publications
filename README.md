**Hanyoung Jang**  
Lab Leader @ Graphics AI Lab, NCSOFT  
Ph.D.  
ncdrjang@ncsoft.com

# Bio-sketch
Dr. Hanyoung Jang is the Lab Leader of the Graphics AI Lab of NCSOFT, a global game company. He has researched in the fields of robotics, General-purpose computing on graphics processing units (GPGPU), computer graphics, and AI. Currently, his primary research interest lies in creating natural character animation through deep learning.

# History
* 2022 ~ Present: Head of Graphics AI Lab, **NCSOFT**
* 2017 ~ 2022: Team Leader @ Game AI Team, **NCSOFT**
* 2012 ~ 2017: AI Researcher @ AI Lab, **NCSOFT**  
* 2005 ~ 2012: Ph.D. & M.S. course @ Media Lab., **Korea University**
* 2000 ~ 2005: B.S course @ **Korea University**


# International Papers
* Ji Hyun Roh, Seong Uk Kim, Hanyoung Jang, Yeongho Seol, Jongmin Kim, [**Interactive Facial Expression Editing with Non-linear Blendshape Interpolation**](https://cse.kangwon.ac.kr/cse/community/news.do?mode=view&articleNo=373315&title=%5B%EB%8F%99%EC%A0%95%5D%EA%B9%80%EC%A2%85%EB%AF%BC+%EA%B5%90%EC%88%98%3B+EUROGRPAHICS+2022+short+paper+%EB%85%BC%EB%AC%B8+%EB%B0%9C%ED%91%9C++), ***EUROGRAPHICS 2022*** *(Short Paper)*, April 25-29, 2022, Reims, France
* Seong Uk Kim, Hanyoung Jang, Hyeonseung Im, Jongmin Kim, [**Human Motion Reconstruction using Deep Transformer Networks**](https://www.sciencedirect.com/science/article/abs/pii/S0167865521002245), ***Pattern Recognition Letters*** *(SCIE)*, 150, October 2021, pp 162-169
* Seong Uk Kim, Hanyoung Jang, Jongmin Kim, [**A Robust Low-cost Mocap System with Sparse Sensors**](https://dl.acm.org/doi/10.1145/3415264.3425463), ***SIGGRAPH ASIA 2020*** *(Poster Session)*, November 18-20, Online Event
* Seong Uk Kim, Hanyoung Jang, Jongmin Kim, [**A variational U-Net for motion retargeting** (Best Paper Nominee)](https://onlinelibrary.wiley.com/doi/abs/10.1002/cav.1947), ***CASA 2020*** *(SCIE)*, October 13-15, Bournemouth, United Kingdom
* Moonwon Yu, Byungjun Kwon, Jongmin Kim, Shinjin Kang, Hanyoung Jang, [**Fast Terrain-Adaptive Motion Generation using Deep Neural Networks**](https://www.youtube.com/watch?v=4DpwvWe9hOM), ***SIGGRAPH ASIA 2019*** *(Technical Breifs)*, November 17-20, 2019, Brisbane, Austraila
* Seong Uk Kim, Hanyoung Jang, Jongmin Kim, [**Human Motion Denoising Using Atention-Based Bidirectional Recurrent Neural Network**](https://www.youtube.com/watch?v=-E_CfjWCJ6A), ***SIGGRAPH ASIA 2019*** *(Poster Session)*, November 17-20, 2019, Brisbane, Austraila
* Hanyoung Jang, Byungjun Kwon, Moonwon Yu, Seong Uk Kim, Jongmin Kim, [**A Variational U-Net for Motion Retargeting**](https://www.youtube.com/watch?v=Kv2ayFELxHg&t=95s), ***SIGGRAPH ASIA 2018*** *(Poster Session)*, Decenber 4-7, 2018, Tokyo, Japan
* Byungjun Kwon, Moonwon Yu, Hanyoung Jang, [**Deep Motion Transfer without Big Data**](https://www.youtube.com/watch?v=hrARRDrawIQ&t=15s), ***SIGGRAPH 2018*** *(Poster Session)*, August 12-16, 2018, Vancouver, BC, Canada
* HyeongYeop Kang, Hanyoung Jang, Chang-Sik Cho, JungHyun Han, [**Multi-resolution Terrain Rendering with GPU Tessellation**](https://www.youtube.com/watch?v=aVPHk1EA0Ko), ***The Visual Computer*** *(SCIE)*, 31, 4, April 2015, pp 455–469
* Yonghyun Jo, Hanyoung Jang, Yeon-Ho Kim, Joon-Kee Cho, Hadi Moradi, and JungHyun Han, [**Memory-efficient Real-time Map Building using Octree of Planes and Points**](https://www.youtube.com/watch?v=d0js2XKaqkQ), ***Advanced Robotics*** *(SCIE)*, Vol. 27, No. 4, March 2013, pp. 301-308
* Hanyoung Jang, JungHyun Han, [**GPU-optimized Indirect Scalar Displacement Mapping**](https://www.youtube.com/watch?v=RwkKa0xxhhM), ***Computer-Aided Design*** *(SCIE)*, 45, 2, February 2013, pp 517-522
* Hanyoung Jang, JungHyun Han, [**Feature-Preserving Displacement Mapping With Graphics Processing Unit (GPU) Tessellation**](https://www.youtube.com/watch?v=yqj19bQ2QxE), ***Computer Graphics Forum*** *(SCIE)*, 31, 6, September 2012, pp 1880-1894  
* Kien T. Nguyen, Hanyoung Jang, JungHyun Han, [**Image-space Hierarchical Coherence Buffer**](https://www.youtube.com/watch?v=UOuYMOVGgms), ***The Visual Computer*** *(SCIE)*, 27, 6-8, June 2011, pp 759-768  
* Yonghyun Jo, Hanyoung Jang, Yeon-Ho Kim, Joon-Kee Cho, Hyoung-Ki Lee, Young Ik Eom, JungHyun Han, **Cooperation of CPU and GPU Programs for Real-time 3D Map Building**, ***ICSOFT 2011*** *: International Conference on Software and Data Technologies*, July 18-21, 2011, Seville, Spain
* Kien T. Nguyen, Hanyoung Jang, JungHyun Han, [**Layered Occlusion Map for Soft Shadow Generation**](https://www.youtube.com/watch?v=hWTeIFwjPs0), ***The Visual Computer*** *(SCIE)*, 26, 12, December 2010, pp 1497-1512  
* Hanyoung Jang, Dong-young Kim, JungHyun Han, **Feature-Preserving Deformation of Tessellated Surfaces**, ***I3D 2010*** *: ACM Symposium in Interactive 3D Graphics and Games (Poster Session)*, February 19-21 2010, Hyatt Regency Bethesda, Washington DC, USA  
* Hanyoung Jang, JungHyun Han, **3D Displacement Map based on Surface Curvature**, ***Pacific Graphics 2009*** *: Pacific Conference on Computer Graphics and Applications*, October 7-9 2009, Zeju, Korea, pp 55-58  
* Sukhan Lee, Hadi Moradi, Daesik Jang, Hanyoung Jang, Eunyoung Kim, Phuoc Le Minh, JungHyun Han, **Toward Human-like Real-time Manipulation: from Perception to Motion Planning**, ***Advanced Robotics*** *(SCIE)*, 22, 9, August 2008, pp 983-1005  
* Hanyoung Jang, JungHyun Han, **Fast Collision Detection using the A-buffer**, ***The Visual Computer*** *(SCIE)*, 24, 7, July 2008, pp 659-667  
* Han-Young Jang, Hadi Moradi, Phuoc Le Minh, Sukhan Lee, JungHyun Han, **Visibility-based Spatial Reasoning for Object Manipulation in Cluttered Environments**, ***Computer-Aided Design*** *(SCIE)*, 40, 4, April 2008, pp 422-438  
* Hanyoung Jang, JungHyun Han, [**Deformable Model Collision Detection using A-Buffer**](https://www.youtube.com/watch?v=FqbUcyOA80U), ***I3D 2008*** *: ACM Symposium in Interactive 3D Graphics and Games (Poster Session)*, February 15-17 2008, Redwood City, CA, USA  
* Han-Young Jang, TaekSang Jeong, JungHyun Han, **Image-Space Collision Detection Through Alternate Surface Peeling**, ***ISCV 2007*** *: International Symposium on Visual Computing (LNCS binding)*, November 26-28 2007, Reno, NV, USA  
* Han-Young Jang, Hadi Moradi, Sukhan Lee, Daesik Jang, Eunyoung Kim, JungHyun Han, **A Graphics Hardware-based Accessibility Analysis for Real-time Robotic Manipulation**, ***DCDIS 2006*** *: Dynamics of Continuous, Discrete & Impulsive Systems, Series B : Applications & Algorithms (LNCS binding)*, 14, S1, Jun 2007, pp 97-106  
* Han-Young Jang, TaekSang Jeong, JungHyun Han, **GPU-based Image-space Approach to Collision Detection among Closed Objects**, ***Pacific Graphics 2006*** *: Pacific Conference on Computer Graphics and Applications (Poster Session)*, October 11-13 2006, Taipei, Taiwan, pp 242-251  
* Han-Young Jang, Hadi Moradi, Suyeon Hong, Sukhan Lee, JungHyun Han, **Spatial Reasoning for Real-time Robotic Manipulation**, ***IROS 2006*** *: IEEE/RSJ International Conference on Intelligent Robots and Systems*, October 9-15 2006, Beijing, China  
* Han-Young Jang, Hadi Moradi, Sukhan Lee, JungHyun Han, **A Visibility-based Accessibility Analysis of the Grasp Points for Real-time Manipulation**, ***IROS 2005*** *: IEEE/RSJ International Conference on Intelligent Robots and Systems*, August 2-6 2005, Edmonton, Alberta, Canada  
* Han-Young Jang, Sukhan Lee, Daesik Jang, Eunyoung Kim, Hadi Moradi, JungHyun Han, **A Hardware-based Accessibility Analysis for Real-time Robotic Manipulation**, ***ICIC 2005*** *: 2005 International Conference on Intelligent Computing (LNCS binding)*, August 23-26, Hefei, China  

# International Patents
* **US10957087(2021.03.23), JP6902071(2021.06.22), CN110782478B(2023.12.15)** MOTION SYNTHESIS APPARATUS AND MOTION SYNTHESIS METHOD
* **US9959670B2(2018-05-01)** Method for rendering terrain

# Domestic Patenets
* (PCT출원신청 2건) 캐릭터의 애니메이션을 생성하기 위한 전자 장치, 방법, 및 비일시적 컴퓨터 판독 가능 저장 매체
* **10-2022-0159096** *(출원중)* 동작 생성 모델 학습 장치 및 동작 생성 모델 학습 방법(DEVICE AND METHOD FOR GENERATING MOTIONS MODEL)
* **10-2021-0103370** *(출원중)* 신경망을 이용하여 사용자 입력에 대응하는 시각적 객체의 상태를 식별하기 위한 전자 장치, 방법, 및 비일시적 컴퓨터 판독가능 저장 매체(ELECTRONIC DEVICE, METHOD, AND NON-TRANSITORY COMPUTER READABLE STORAGE MEDIUM FOR IDENTIFYING STATE OF VISUAL OBJECT CORRESPONDING TO USER INPUT USING NEURAL NETWORK)
* **10-2020-0119508** *(출원중)* 데이터 노이즈 제거 장치 및 데이터 노이즈 제거 방법(DENOISING DEVICE AND DENOISING METHOD FOR DATA NOISE)
* **10-2019-0105203** *(출원중)* 딥 뉴럴 네트워크를 이용한 애니메이션 생성 장치 및 애니메이션 생성 방법(DEVICE AND METHOD FOR GENERATING ANIMATION USING DEEP NEURAL NETWORK)
* **10-2609293 (2023.11.29)** 게임 동작 결정 장치 및 방법(APPARATUS AND METHOD FOR DETERMINING GAME ACTION)
* **10-2584901 (2023.09.26)** 이벤트 정보 송신 장치 및 방법, 이벤트 정보 출력 장치 및 방법(APPARATUS AND METHOD FOR SENDING EVENT INFORMATION, APPARATUS AND METHOD FOR DISPLAYNG EVENT INFORMATION)
* **10-2560754 (2023.07.24)** 모션 리타겟팅 장치 및 모션 리타겟팅 방법(APPARATUS AND METHOD FOR MOTION RETARGETING)
* **10-2551254 (2023.06.29)** 게임 컨텐츠 공유 체험 서비스 제공 방법 및 이를 위한 컴퓨터 프로그램(Method and computer program for providing a service of sharing a game)
* **10-2543650 (2023.06.09)** 모션 합성 장치 및 모션 합성 방법(APPARATUS AND METHOD FOR MOTION SYNTHESIZATION)
* **10-2535266 (2023.05.17)** 동작 생성 모델 학습 장치 및 방법, 동작 생성 장치 및 방법(APPARATUS AND METHOD FOR LEARNING MOTION GENERATION MODEL, APPARATUS AND METHOD FOR GENRATING MOTION)
* **10-2096856 (2020.03.30)** 게임 매칭 장치 및 방법(APPARATUS AND METHOD FOR MATCHING GAME)
* **10-1713028 (2017.02.28)** 위치 결정 장치 및 위치 결정 방법 (POSITIONING APPARATUS AND POSITIONING METHOD)
* **10-1639351 (2016.07.07)** 웨어러블 입력 시스템 및 모션 인식 방법 (Wearable input system and method for recognizing motion)
* **10-1629371 (2016.06.03)** 온라인 게임의 동적 난이도 조절 시스템, 방법 및 컴퓨터 프로그램 (System, method, and computer program recorded on media for dynamically adjusting level of difficulty of online-game)
* **10-1619576 (2016.05.02)** 컴퓨터 상에서 실행되는 랭킹의 시각화 방법 및 이를 실행하는 컴퓨터 (Computer implemented visualization method of ranking over multiple level of ranking map)
* **10-1611672 (2016.04.05)** 지도상의 영역 크기에 의한 랭킹 표시방법 (Computer implemented method of displaying ranking in a form of territory map)
* **10-1611673 (2016.04.05)** 스코어 및 거리에 기반한 랭킹 계산 방법 (Computer implemented method of calculating ranking based on score and distance)
* **10-1555426 (2015.09.17)** 지형 렌더링 방법 및 장치 (METHOD AND APPARATUS FOR RENDERING TERRAIN)

# Etc.
* GDC 2023, Digital TJ 소개
* GDC 2019, [Deep Learning based Large Scale Inverse Kinematics Accelerated by Intel OpenVINO Toolkits](https://www.aitimes.kr/news/articleView.html?idxno=13497)
* Intel White Paper, 2018-05-30, Leveraging Intel DLDT for Inference experience in XEON

# Talk
* 글로벌게임허브센터, 2024-12-18, 게임 제작 효율화 기술 및 적용 사례
* 고려대학교 AUAI, 2024-11-28, AI와 그래픽스
* 고려대학교 가상증강현실연구소 콜로퀴움, 2024-10-16, NC Research 소개 및 그래픽스AI 연구 사례
* 고려대학교 정보통신대학, 2024-04-01, 게임회사의 그래픽스 R&D
* 경희대학교 취창업토크콘서트, 2022-02-11, 게임회사의 R&D
* 덕성여자대학교, 2018-11-28, NCSOFT의 캐릭터 애니메이션 R&D 소개
* 이화여자대학교, 2018-11-07, NCSOFT의 캐릭터 애니메이션 R&D 소개

